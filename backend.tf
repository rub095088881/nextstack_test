#Set S3 backend for persisting TF state file remotely, ensure bucket already exits
# And that AWS user being used by TF has read/write perms


terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }





  backend "s3" {
    region  = "us-east-1"
    profile = "default"
    key     = "terraformstatefile"
    bucket  = "my-buckett-ncjdsnck5dfsdfdcdcfdffvfdsfsdvjkjkfsdfsdf"
  }
} # Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
}