

#Create key-pair for logging into EC2 in us-east-1

resource "tls_private_key" "ssh" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "master-key" {
  #provider   = aws.region-master
  key_name   = "nodeapp-key"
  public_key = tls_private_key.ssh.public_key_openssh

}


resource "local_file" "private_key" {
  content         = tls_private_key.ssh.private_key_pem
  filename        = "local_private_key.pem"
  file_permission = "0600"
  #depends_on = [tls_private_key.ssh, aws_key_pair.master-key]
}


#Create and bootstrap EC2 in us-east-1
resource "aws_instance" "nodeapp-master" {
  #provider                    = aws.region-master
  ami                         = "ami-0149b2da6ceec4bb0"
  instance_type               = var.instance-type
  key_name                    = aws_key_pair.master-key.key_name
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.nodeapp-sg.id]
  subnet_id                   = aws_subnet.subnet_1.id
  user_data                   = file("user-data.sh")

  tags = {
    Name = "nodeapp_master_tf"
  }




 /*  provisioner "file" {
    source      = "index.js"
    destination = "index.js"

    connection {
      type        = "ssh"
      host        = self.public_ip
      user        = "ubuntu"
      private_key = file("local_private_key.pem")
    }
  }



  provisioner "file" {
    source      = "package.json"
    destination = "package.json"
    connection {
      type        = "ssh"
      host        = self.public_ip
      user        = "ubuntu"
      private_key = file("local_private_key.pem")
    }
  }

  provisioner "file" {
    source      = "package-lock.json"
    destination = "package-lock.json"
    connection {
      type        = "ssh"
      host        = self.public_ip
      user        = "ubuntu"
      private_key = file("local_private_key.pem")
    }

  } */

 





  depends_on = [aws_main_route_table_association.set-master-default-rt-assoc]
}




 