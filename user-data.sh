#! /bin/bash


#curl -fsSL https://deb.nodesource.com/setup_14.x | sudo -E bash -
#sudo apt-get install -y nodejs
#sudo npm install pm2@latest -g

# Install docker

sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install -y docker-ce
sudo usermod -aG docker ubuntu
sudo git clone https://gitlab.com/rub095088881/nextstack_test.git
cd /nextstack_test/
docker build . -t test:1
docker run -t -d -p 3001:3001 test:1
sudo mkdir hello-from-test
#pm2 start npm -- start