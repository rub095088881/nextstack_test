FROM node:latest
# Create app directory
WORKDIR /app
COPY . ./

RUN npm install
RUN npm install pm2@latest -g
 # Bundle app source
COPY . .
EXPOSE 3001
ENTRYPOINT [ "npm", "run", "start" ]
